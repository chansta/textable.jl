# TeXTable.jl

## Description

Package to generate a basic tabular syntax in LaTeX. It is still work in progress so be aware of bugs.  

A example on usage can be found in the **test** folder. 


## Installation

Package can be installed via the usual **add** command in [Pkg](https://github.com/JuliaLang/Pkg.jl) with the URL. 


