module TeXTable

using Printf

export parenthesis, TeXTableObj, genTeXTable, formatTable, matrix2TeX, matrix2TeXMatrix, matrixType, bmatrix, pmatrix, vmatrix, Bmatrix, Vmatrix

include("TeXType.jl")
include("TeXfn.jl")

end # module TeXTable
