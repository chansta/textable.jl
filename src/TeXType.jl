@enum axis row column
@enum matrixType bmatrix pmatrix vmatrix Bmatrix Vmatrix
"""

# TeXTableObj

## Description

    An structure (object) containing the elements of a TeX Table. 

## Elements/Members

    rowlabels::Union{Nothing, Array{String, 1}}: A vector of row labels. Can be of the type nothing when row labels are not needed. 
    collabels::Union{Nothing, Array{String,1}}: A vector of column labels. Can be of the type nothing when column lables are not needed. 
    body::Union{Nothing, Array{String,2}}: The contect of the table. It is assumed the elements of the table are of the type String. One can use the function formatTable to convect a matrix of Float64 into a matrix of String. 
    emptyrowlabel::Bool: If true, then every second row will have an empty label. This increases the number of rows from `m` to `2m`. One example of such cases is having standard errors directly underneath the coefficeint estimates. See also formatTable. 

"""
struct TeXTableObj
    rowlabels::Union{Nothing, Array{String, 1}}
    collabels::Union{Nothing, Array{String,1}}
    body::Union{Nothing, Array{String,2}}
    emptyrowlabel::Bool
end

"""
# parenthesis

## Description

    A structure to store the parenthesis for matrix2TeX function 

"""
struct parenthesis
    left::String
    right::String
    NotSym::String
end

MatrixWithNothing = Union{Array{Float64, 2}, Array{Union{Nothing, Float64}, 2}}

defaultp = parenthesis("(", ")", "-")


