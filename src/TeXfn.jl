"""

## Usage

formatTable(df::Array{Float64,2}, df2::Union{Nothing,Array{Float64,2}}=nothing ; leftB::String="(", rightB::String=")")::Union{Nothing, Array{String,2}}

## Description

This function primarily convert a matrix of Float64 to a matrix of String to be used in the construction of a TeXTable object. However, it can also create the matrix of String by merging two matricies of Float64, where the odd number rows in the resulting matrix belong to the first matrix and the even number rows belong to the second matrix. One use case of this is the regression output where each standard error appears directly under its coefficient estimate. Parenthesis can also be put around all entries in the even number rows. 

## Inputs

    df::Array{Float64,2}: A matrix of Float64. The content of the table. 
    df2::Array{Float64,2}: (Optional). The second matrix of Float64, to be merged with df. 
    p::parenthesis: Parenthesis instance containing the left and right parenthesis for the entries in df2. 
"""
function formatTable(df::MatrixWithNothing, df2::Union{Nothing, MatrixWithNothing}=nothing; bracket::parenthesis=defaultp, digits::Int64=4)
    m,n = size(df)
    result = Array{String,2}(undef, 2*m,n)
    if df2 == nothing
	dftemp = [  
		  [
		    isnothing(a) ? bracket.NotSym : (
					  abs(a)<10^(-float(digits)) ? @sprintf("%.3e", a) : string(round(a,digits=digits)) 
					 ) 
			for a in l] for l in eachcol(df)
		]
	result = hcat(dftemp...)
	return result
    else
	p,q = size(df2)
	if (m,n)!=(p,q)
	    println("The two matricies do not confirm")
	    return nothing
	else
	    k = 1
	    for i in 1:m
		result[k,:] = [
			       isnothing(a) ? bracket.NotSym : (
						     abs(a) < 10^(-float(digits)) ? @sprintf("%.3e", a) : string(round(a, digits=digits))
						     )
				for a in df[i, :] 
			       ]
		result[k+1,:] = [ isnothing(a) ? bracket.NotSym : (
							abs(a) < 10^(-float(digits)) ? bracket.left*@sprintf("%.3e", a)*bracket.right : bracket.left*string(round(a, digits=digits))*bracket.right 
							) 
				for a in df2[i,:] 
				]
		k = k+2
	    end
	    return result
	end
    end
end

"""
## Usage

    genTeXTable(df::TeXTableObj, fname::String, cformater::String="c")::String

## Description
    
    Generate the LaTeX source and save it to a file given the TeXTable Object, the file name and the column formatter.

## Inputs

    df::TeXTableObj. TeXTableObj containing all the data for generating the LaTeX table. 
    fname::String. The name of the file to save the output string. 
    cformater::String. The LaTeX column formatter. Default is "c" but can take something like "p{4cm}"

## Output
    s::String. LaTeX source of the table. The source will be saved to the file fname (see Inputs). 

"""
function genTeXTable(df::TeXTableObj, fname::String, cformater::String="c")::String
    m,n = size(df.body)
    prime = repeat(cformater,n)
    if df.rowlabels != nothing
	prime = cformater*"|"*prime
	if df.emptyrowlabel == false
	    tempdf = hcat(reshape(df.rowlabels, (m,1)), df.body)
	else
	    m2 = Int(m/2)
	    temprowlabels = reshape(vcat(reshape(df.rowlabels,(1,m2)), reshape(repeat([""],m2), (1,m2))), (m, 1))
	    tempdf = hcat(temprowlabels, df.body)
	end
    else 
	tempdf = df.body
    end
    s = ""
    for (i,r) in enumerate(eachrow(tempdf))
	if i==1
	    s = join(r, "&")*"\\\\ \n"
	else
	    s = s*join(r, "&")*"\\\\ \n"
	end
    end
    header = "\\begin{tabular}{"*prime*"}\n"
    footer = "\\end{tabular}\n" 
    if df.collabels != nothing
	col = "&"*join(df.collabels, "&")*"\\\\ \\midrule \n"
	s = header*col*s*footer
    else
	s=header*s*footer
    end
    open(fname, "w") do f
	write(f, s)
    end
    return s
end 

"""
## Usage

    matrix2TeX(df::Array{Float64,2}, df2::Union{Nothing, Array{Float64,2}}=nothing; columns::Union{Nothing, Array{String,1}}=nothing, rows::Union{Nothing, Array{String,1}}=nothing, bracket::parenthesis=defaultp, file::String="output.tex")::String

## Description
    
    The main function that converts a given matrix into a LaTeX table. Users can also provide a second matrix. In that case the two matrices will merge in such a way that all even number rows will be sourced from the second matrix with each entry surrounded by a given set of paranethesis as given by the object parenthesis. A typical use case is a table presenting regression result where the standard error will be presented underneath its coefficient estimate, with specified parenthesis. 

## Inputs

    df::Array{Float64,2}. The data matrix. 
    df2::Array{Float64,2}. Optional. If provided, all even number rows in the resulting table will be sourced from this matrix. 
    columns::Array{String,1}. Vector of column labels. 
    rows::Array{String,1}. Vector of row labels.
    bracket::parenthesis. An instance of the object parenthesis. The left and right parenthesis used in df2. 
    file::String. The filename of the LaTeX table. The generated LaTeX source will be saved to that file.  


## Output

    s::String. The LaTeX source and the string will also be saved to the file given by file (see Inputs). 

"""
function matrix2TeX(df::MatrixWithNothing, df2::Union{Nothing, MatrixWithNothing}=nothing; columns::Union{Nothing, Array{String,1}}=nothing, rows::Union{Nothing, Array{String,1}}=nothing, bracket::parenthesis=defaultp, digits::Int64=4, file::String="output.tex")::String
    if df2 == nothing
	body = formatTable(df, digits=digits)
	TTable=TeXTableObj(rows, columns, body, false)
    else
	body = formatTable(df,df2,bracket=bracket, digits=digits)
	TTable=TeXTableObj(rows, columns, body, true)
    end
    s = genTeXTable(TTable, file)
    return s
end

"""
## Usage 

    matrix2TeXMatrix(m::Union{Array{Float64,1}, Array{Float64,2}, Array{Int64,1}, Array{Int64,2}}, type::matrixType=bmatrix, file::String="output.tex")

## description 
    
    A function that conver a matrix (or vector) in Julia into LaTeX code. 

## Input

    m::Union{Array{Float64,1}, Array{Float64,2}, Array{Int64,1}, Array{Int64,2}}. The matrix or vector that one wishes to convert. 
    type::matrixType. Can be one of pmatrix, bmatrix, vmatrix, Bmatrix and Vmatrix. 
    file::String. The file name. Default is None

## Output
    final:String. The corresponding LaTeX code for the matrix.

"""
function matrix2TeXMatrix(m::Union{Array{Float64,1}, Array{Float64,2}, Array{Int64,1}, Array{Int64,2}}, type::matrixType=bmatrix, file::String="None")
    s = join([join(string.(r), "&") for r in eachrow(m)], "\\\\ \n")*"\n"
    header = "\\begin{$type} \n"
    footer = "\\end{$type} \n"
    final = header*s*footer
    if file != "None"
	open(file, "w") do f
	    write(f,final)
	end
    end
    return final
end

