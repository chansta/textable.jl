using Distributions
using TeXTable

dist = Normal()
df = rand(dist, (3,2))
df_se = abs.(rand(dist, (3,2)))

p1 = parenthesis("[", ")")
columnlabels = @. "M"*["1", "2"]
rowlabels = @. "X"*["1", "2", "3"]
s = matrix2TeX(df,df_se, rows=rowlabels, columns=columnlabels, file="overall.tex")
A = [1 2 3; 4 5 6]
s2 = matrix2TeXMatrix(A, bmatrix, "matrix.tex")
